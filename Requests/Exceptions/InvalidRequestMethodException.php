<?php
namespace Maksatech\Http\Requests\Exceptions;

use Exception;
use Throwable;

/**
 * Class InvalidRequestMethodException
 * @package Maksatech\Http\Requests\Exceptions
 */
class InvalidRequestMethodException extends Exception
{
    /**
     * InvalidRequestMethodException constructor.
     * @param string $methodName
     * @param Throwable|null $previous
     */
    public function __construct(string $methodName, Throwable $previous = null)
    {
        parent::__construct('The \''.$methodName.'\' request method is invalid', 0, $previous);
    }
}