<?php
namespace Maksatech\Http\Requests\Exceptions;

use Exception;
use Throwable;

class InvalidDependencyConfigException extends Exception
{
    /**
     * @param Throwable|null $previous
     */
    public function __construct(string $message, Throwable $previous = null)
    {
        parent::__construct('Dependency config is not valid: ' . $message, 0, $previous);
    }
}