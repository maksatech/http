<?php
namespace Maksatech\Http\Requests\Exceptions;

use Exception;
use Throwable;

/**
 * Class DuplicatedRouteException
 * @package Maksatech\Http\Requests\Exceptions
 */
class DuplicatedRouteException extends Exception
{
    /**
     * DuplicatedRouteException constructor.
     * @param string $path
     * @param Throwable|null $previous
     */
    public function __construct(string $path, Throwable $previous = null)
    {
        parent::__construct('The route \''.$path.'\' is duplicated', 0, $previous);
    }
}