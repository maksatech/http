<?php
namespace Maksatech\Http\Requests;

use Maksatech\Containers\LanguageInterface;
use Maksatech\Containers\SiteInterface;

/**
 * Class Request
 * @package Maksatech\Http\Requests
 */
class Request extends \Symfony\Component\HttpFoundation\Request
{

    /**
     * @var string
     */
    protected string $requestPath;

    /**
     * @var string
     */
    protected string $requestPathDecoded;

    /**
     * @var SiteInterface|null
     */
    protected $site = null;

    /**
     * @var LanguageInterface|null
     */
    protected $language = null;

    /**
     * Sets the request path.
     *
     * @param string $path The request path
     */
    public function setRequestPath(string $path): void
    {
        $this->requestPath = $path;
        $this->requestPathDecoded = urldecode($path);
    }

    /**
     * Returns the requested PATH (path and query string).
     *
     * @param bool $decoded The request path decode
     * @return string The raw PATH (i.e. not URI decoded)
     */
    public function getRequestPath(bool $decoded = false): string
    {
        return ($decoded ? $this->requestPathDecoded : $this->requestPath);
    }

    /**
     * Returns the requested SITE (SITE object).
     *
     * @return SiteInterface|null The raw SITE
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Sets the request site.
     *
     * @param SiteInterface $site The request site
     */
    public function setSite(SiteInterface $site): void
    {
        $this->site = $site;
    }

    /**
     * Returns the requested LANGUAGE (LANGUAGE object).
     *
     * @return LanguageInterface|null The raw LANGUAGE
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Sets the request language.
     *
     * @param LanguageInterface $language
     */
    public function setLanguage(LanguageInterface $language): void
    {
        $this->language = $language;
    }
}