<?php

namespace Maksatech\Http\Requests;

use Maksatech\Core\Core;
use Maksatech\Http\Requests\Exceptions\InvalidDependencyConfigException;

abstract class DependencyRouter extends Core
{
    protected array $config;

    /**
     * @param array $config
     * @throws InvalidDependencyConfigException
     */
    public function __construct(array $config)
    {
        parent::__construct();

        if(array_key_exists('class', $config) && !is_null($config['class']) && !class_exists($config['class'])) {
            throw new InvalidDependencyConfigException('`class` setting contains a non-existent class ' . $config['class']);
        }

        $this->config = [
            'class' => $config['class'] ?? null
        ];
    }

    /**
     * @return string|null
     */
    public function getClass(): ?string
    {
        return $this->config['class'];
    }
}