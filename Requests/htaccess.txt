AddDefaultCharset utf-8
AddCharset utf-8 *
RewriteEngine On
<IfModule mod_charset.c>
    CharsetSourceEnc utf-8
    CharsetDefault utf-8
</IfModule>
Options +FollowSymlinks

RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_URI} !=/favicon.ico
RewriteRule ^(.*)$ index.php [L]