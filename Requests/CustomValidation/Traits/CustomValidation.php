<?php
namespace Maksatech\Http\Requests\CustomValidation\Traits;

use Illuminate\Contracts\Validation\Validator;
use Maksatech\Http\Requests\CustomValidation\Exceptions\CustomValidationException;

/**
 * Trait CustomValidation
 * @package Maksatech\Http\Requests\CustomValidation\Traits
 */
trait CustomValidation
{
    /**
     * Reloaded - throw custom exception
     *
     * @param Validator $validator
     * @throws CustomValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new CustomValidationException($validator);
    }
}