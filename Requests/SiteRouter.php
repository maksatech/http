<?php

namespace Maksatech\Http\Requests;

use Maksatech\Containers\SiteInterface;
use Maksatech\Http\Requests\Exceptions\InvalidSiteConfigException;
use Maksatech\Http\Requests\Exceptions\InvalidDependencyConfigException;
use Maksatech\Views\Rendering\UnknownSite;
use ReflectionClass;
use ReflectionException;

class SiteRouter extends DependencyRouter
{
    protected string $host;

    /**
     * @param array $config
     * @param string $host
     * @throws InvalidDependencyConfigException
     * @throws InvalidSiteConfigException
     * @throws ReflectionException
     */
    public function __construct(array $config, string $host)
    {
        parent::__construct($config);

        if(array_key_exists('class', $config) && !is_null($config['class'])) {
            $reflection = new ReflectionClass($config['class']);

            if(!$reflection->implementsInterface(SiteInterface::class)) {
                throw new  InvalidSiteConfigException($reflection->getName()." class not implements " . SiteInterface::class);
            }
        }

        $this->host = $host;
    }

    /**
     * @return SiteInterface
     */
    public function load(): SiteInterface
    {
        if(is_null($this->getClass())) {
           return UnknownSite::loadByDomain($this->host);
        }

        $site = $this->getClass()::loadByDomain($this->host);

        if(is_null($site)) {
            return UnknownSite::loadByDomain($this->host);
        }

        return $site;
    }
}