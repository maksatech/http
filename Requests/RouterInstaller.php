<?php
namespace Maksatech\Http\Requests;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;
use Maksatech\Core\Core;
use Maksatech\Core\Installer;
use ReflectionException;
use ReflectionMethod;

/**
 * Class RouterInstaller
 * @package Maksatech\Http\Requests
 */
class RouterInstaller extends Router
{

    /**
     * RouterInstaller constructor.
     * @throws ReflectionException
     */
    function __construct()
    {
        parent::__construct();
    }

    function __destruct()
    {
        parent::__destruct();
    }

    /**
     * @param PackageEvent|null $event
     * @throws ReflectionException
     */
    public static function init(PackageEvent $event = null)
    {
        Installer::init($event);

        $config = self::getConfig();

        if($config->hasFile())
            $config->load();

        $method = new ReflectionMethod(get_class($config),'put');

        if($method->isProtected() || $method->isPrivate())
            $method->setAccessible(true);

        $method->invokeArgs($config,[[
            'router_maps_dirs' => [
                'core/config/http/request/router'
            ],
            'language' => [
                'class' => null,
                'pattern' => LanguageRouter::ROUTING_PATTERN_HOST
            ],
            'site' => [
                'class' => null
            ]
        ]]);

        $coreConfig = Core::getConfig();
        if($coreConfig->hasFile()) {
            $coreConfig->load();
            if($coreConfig->has('web_root_dir')) {
                if(file_exists(self::webRootDirPath()))
                    file_put_contents(self::webRootDirPath().'/.htaccess',file_get_contents(__DIR__.'/htaccess.txt'));
            }

            if($config->has('router_maps_dirs')) {
                $routerMapsDirs = $config->get('router_maps_dirs');
                if(is_array($routerMapsDirs)) {
                    foreach ($routerMapsDirs as $routerMapsDir) {
                        if(!file_exists(self::systemRootDirPath().'/'.$routerMapsDir))
                            mkdir(self::systemRootDirPath().'/'.$routerMapsDir,0755,true);
                    }
                }
            }
        }
    }

}