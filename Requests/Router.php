<?php
namespace Maksatech\Http\Requests;

use Illuminate\Foundation\Application;
use Illuminate\Validation\DatabasePresenceVerifier;
use Illuminate\Validation\Validator;
use Maksatech\Containers\DatabaseContainerInterface;
use Maksatech\Containers\InstancesContainerInterface;
use Maksatech\Containers\LanguageInterface;
use Maksatech\Containers\LocaleContainerInterface;
use Maksatech\Containers\RequestContainerInterface;
use Maksatech\Containers\SiteContainerInterface;
use Maksatech\Containers\ViewsContainerInterface;
use Maksatech\Containers\WithDatabaseContainerInterface;
use Maksatech\Containers\WithInstancesContainerInterface;
use Maksatech\Containers\WithLocaleContainerInterface;
use Maksatech\Containers\WithRequestContainerInterface;
use Maksatech\Containers\WithSiteContainerInterface;
use Maksatech\Containers\WithViewsContainerInterface;
use Maksatech\Core\Core;
use Maksatech\Http\Controllers\AbstractController;
use Maksatech\Http\Middleware\Exceptions\MiddlewareClassNotFoundException;
use Maksatech\Http\Middleware\Exceptions\MiddlewareMethodNotFoundException;
use Maksatech\Http\Requests\CustomValidation\Exceptions\CustomValidationException;
use Maksatech\Http\Requests\Exceptions\DuplicatedRouteException;
use Maksatech\Http\Requests\Exceptions\InvalidRequestMethodException;
use Maksatech\Http\Responses\Response;
use Maksatech\Views\Rendering\Theme;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Yaml\Yaml;
use Illuminate\Http\Request as IlluminateHttpRequest;
use ReflectionException;
use Exception;
use ReflectionMethod;

/**
 * Class Router
 * @package Maksatech\Http\Requests
 */
class Router extends Core implements WithLocaleContainerInterface, WithDatabaseContainerInterface, WithInstancesContainerInterface, WithRequestContainerInterface, WithSiteContainerInterface, WithViewsContainerInterface
{

    /**
     * @var string
     */
    protected static string $configNamespace = 'core';

    /**
     * @var string
     */
    protected static string $configDir = 'http/request';

    /**
     * @var string
     */
    protected static string $configName = 'router';

    protected string $requestPath;
    protected string $requestPathDecoded;
    protected array $routes;


    public const ALLOWED_METHODS = [
        'post',
        'get',
        'put',
        'delete'
    ];

    /**
     * @var null|LocaleContainerInterface
     */
    protected $localeContainer = null;

    /**
     * @var null|DatabaseContainerInterface
     */
    protected $databaseContainer = null;


    /**
     * @var null|InstancesContainerInterface
     */
    protected $instancesContainer = null;

    /**
     * @var null|RequestContainerInterface
     */
    protected $requestContainer = null;


    /**
     * @var null|SiteContainerInterface
     */
    protected $siteContainer = null;

    /**
     * @var null|ViewsContainerInterface
     */
    protected $viewsContainer = null;

    /**
     * Router constructor.
     * @throws ReflectionException
     * @throws Exception
     */
    function __construct()
    {
        parent::__construct();

        $config = self::getConfig(true);

        $routes = [
            'usual' => [],
            'regular' => []
        ];


        if(!Core::getConfig(true,true)->has('storage_dir') || !file_exists(self::storageDirPath().'/router/routes-'.Core::getCacheString().'.php')) {
            $usedPaths = [];
            $this->routes = [
                'usual' => [],
                'regular' => []
            ];

            $routerMapsDir = [];

            if($config->has('router_maps_dirs'))
                $routerMapsDir = $config->get('router_maps_dirs');

            foreach ($routerMapsDir as $routerMapsDirItem) {
                $routingYmlGlob = glob(self::systemRootDirPath().'/'.$routerMapsDirItem.'/*.routing.yml');
                foreach ($routingYmlGlob as $routingYmlPath) {
                    $routesBundle = Yaml::parse(file_get_contents($routingYmlPath));
                    foreach ($routesBundle as $routeName => $route) {
                        if(isset($route['path']) && isset($route['action'])) {
                            if(array_key_exists('method', $route)) {
                                if(null !== $route['method']) {
                                    if(!is_array($route['method'])) {
                                        $route['method'] = [$route['method']];
                                    }

                                    for($mi = 0; $mi < count($route['method']); $mi++) {
                                        $route['method'][$mi] = mb_strtolower(trim($route['method'][$mi]));
                                        if(!in_array($route['method'][$mi],self::ALLOWED_METHODS)) {
                                            throw new InvalidRequestMethodException($route['method'][$mi]);
                                        }
                                    }
                                }
                            }
                            else {
                                $route['method'] = null;
                            }

                            $vars = [];

                            if(array_key_exists('var', $route)) {
                                foreach ($route['var'] as $routeVarName => $routeVarVal) {
                                    $vars[$routeVarName] = [
                                        'type' => null,
                                        'value' => $routeVarVal,
                                        'position' => null
                                    ];
                                }
                            }

                            $middlewareList = null;

                            if(array_key_exists('middleware', $route)) {

                                if(!is_array($route['middleware'])) {
                                    $route['middleware'] = [$route['middleware']];
                                }

                                foreach ($route['middleware'] as $middlewareItem) {
                                    if(is_array($middlewareItem)) {
                                        foreach($middlewareItem as $routeMiddlewareKey => $routeMiddlewareVal) {
                                            if(is_array($routeMiddlewareVal)) {
                                                for($mj = 0; $mj < count($routeMiddlewareVal); $mj++) {
                                                    if(null === $middlewareList) {
                                                        $middlewareList = [];
                                                    }

                                                    if(is_array($routeMiddlewareVal[$mj]) && array_key_exists('middleware', $routeMiddlewareVal[$mj])) {
                                                        $middlewareList[] = [
                                                            'middleware' => $routeMiddlewareKey.'::'.$routeMiddlewareVal[$mj]['middleware'],
                                                            'var' => $routeMiddlewareVal[$mj]['var'] ?? []
                                                        ];
                                                    }
                                                    else {
                                                        $middlewareList[] = $routeMiddlewareKey.'::'.$routeMiddlewareVal[$mj];
                                                    }
                                                }
                                            }
                                            else {
                                                if(null === $middlewareList) {
                                                    $middlewareList = [];
                                                }
                                                if(is_array($routeMiddlewareVal) && array_key_exists('middleware', $routeMiddlewareVal)) {
                                                    $middlewareList[] = [
                                                        'middleware' => $routeMiddlewareKey.'::'.$routeMiddlewareVal['middleware'],
                                                        'var' => $routeMiddlewareVal['var'] ?? []
                                                    ];
                                                }
                                                else {
                                                    $middlewareList[] = $routeMiddlewareVal;
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        if(null === $middlewareList) {
                                            $middlewareList = [];
                                        }
                                        $middlewareList[] = $middlewareItem;
                                    }
                                }
                            }


                            $route['path'] = str_replace(' ','',trim($route['path']));

                            if($route['path'] !== '*') {
                                $route['path'] = str_replace('//','/',$route['path']);
                            }

                            if($route['path'][mb_strlen($route['path'])-1] === '/') {
                                $route['path'] = mb_substr($route['path'],0,mb_strlen($route['path'])-1);
                            }

                            $path_str = $route['path'];
                            $par_pat = '/\{([a-z_A-Z]+[a-zA-Z_0-9\:]{0,})\}/';
                            $path_str = str_replace('.','\.',$path_str);

                            $pat = $path_str;
                            $pat = '/^'.str_replace('/','\/',$pat).'$/';


                            $preg_vars = [];
                            preg_match_all($par_pat,$path_str,$preg_vars);
                            $preg_vars = $preg_vars[1];

                            if(count($preg_vars) > 0) {
                                $usedPathKey = $pat;
                            }
                            else {
                                $usedPathKey = $route['path'];
                            }

                            if(array_key_exists($usedPathKey, $usedPaths)) {
                                if($route['method'] === $usedPaths[$usedPathKey]) {
                                    throw new DuplicatedRouteException($usedPathKey);
                                }
                                elseif (null === $route['method'] || null === $usedPaths[$usedPathKey]) {
                                    throw new DuplicatedRouteException($usedPathKey);
                                }
                                elseif (count(array_uintersect($route['method'], $usedPaths[$usedPathKey], "strcasecmp")) > 0) {
                                    throw new DuplicatedRouteException($usedPathKey);
                                }
                            }

                            $usedPaths[$usedPathKey] = $route['method'];

                            if(count($preg_vars) > 0) {
                                foreach ($preg_vars as $position => $preg_var) {
                                    $preg_varExplode = explode(":",$preg_var);
                                    if(count($preg_varExplode) > 1) {
                                        if(count($preg_varExplode) > 1 && count($preg_varExplode) < 5) {
                                            if(in_array($preg_varExplode[1],['string','int','float'])) {
                                                $nonzero = false;
                                                $unsigned = false;
                                                for($explIterator = 2; $explIterator < count($preg_varExplode); $explIterator++) {
                                                    if($preg_varExplode[$explIterator] === 'nonzero') {
                                                        if($preg_varExplode[1] === 'string') {
                                                            throw new Exception("Invalid path pattern");
                                                        }
                                                        else {
                                                            if(!$nonzero)
                                                                $nonzero = true;
                                                            else throw new Exception("Invalid path pattern");
                                                        }
                                                    }
                                                    elseif ($preg_varExplode[$explIterator] === 'unsigned') {
                                                        if($preg_varExplode[1] === 'string') {
                                                            throw new Exception("Invalid path pattern");
                                                        }
                                                        else {
                                                            if(!$unsigned)
                                                                $unsigned = true;
                                                            else throw new Exception("Invalid path pattern");
                                                        }
                                                    }
                                                    else throw new Exception("Invalid path pattern");
                                                }
                                                $vars[$preg_varExplode[0]] = [
                                                    'type' => $preg_varExplode[1],
                                                    'position' => $position
                                                ];
                                                switch ($preg_varExplode[1]) {
                                                    case 'int': {
                                                        if($unsigned)
                                                            $sign = "";
                                                        else
                                                            $sign = "[\-]{0,1}+";

                                                        if($nonzero)
                                                            $pat = str_replace("{".$preg_var."}", '+('.$sign.'[1-9]{1,}+[0-9]{0,})', $pat);
                                                        else
                                                            $pat = str_replace("{".$preg_var."}", '+('.$sign.'[1-9]{1,}+[0-9]{0,}|'.$sign.'0)', $pat);

                                                        break;
                                                    }
                                                    case 'float': {
                                                        if($unsigned)
                                                            $sign = "";
                                                        else
                                                            $sign = "[\-]{0,1}+";

                                                        if($nonzero)
                                                            $pat = str_replace("{".$preg_var."}", '+('.$sign.'[1-9]{1,}+[0-9]{0,}|'.$sign.'[1-9]{1,}+[0-9]{0,}+\.+[0-9]{1,}|'.$sign.'0+\.+[0-9]+[1-9]{1,}+[0-9]{0,})', $pat);
                                                        else
                                                            $pat = str_replace("{".$preg_var."}", '+('.$sign.'[1-9]{1,}+[0-9]{0,}|'.$sign.'[1-9]{1,}+[0-9]{0,}+\.+[0-9]{1,}|'.$sign.'0+\.+[0-9]{1,}|'.$sign.'0)', $pat);

                                                        break;
                                                    }
                                                    default:  {
                                                        $pat = str_replace("{".$preg_var."}", '+([^\/]{1,})', $pat);
                                                        break;
                                                    }
                                                }
                                            }
                                            else throw new Exception("Invalid path pattern");
                                        }
                                        else throw new Exception("Invalid path pattern");
                                    }
                                    else {
                                        $vars[$preg_var] = [
                                            'type' => null,
                                            'position' => $position
                                        ];
                                        $pat = str_replace("{".$preg_var."}", '+([^\/]{1,})', $pat);
                                    }
                                }

                                $routes['regular'][$routeName] = [
                                    'path' => $pat,
                                    'action' => $route['action'],
                                    'middleware' => $middlewareList,
                                    'method' => $route['method'],
                                    'var' => $vars
                                ];
                            }
                            else {
                                $routes['usual'][$routeName] = [
                                    'path' => $route['path'],
                                    'action' => $route['action'],
                                    'middleware' => $middlewareList,
                                    'method' => $route['method'],
                                    'var' => $vars
                                ];
                            }
                        }
                    }
                }
            }

            if(Core::getConfig(true,true)->has('storage_dir')) {
                if(file_exists(self::storageDirPath().'/router')) {
                    foreach (glob(self::storageDirPath().'/router/*') as $file) {
                        unlink($file);
                    }
                }
            }

            if(Core::getConfig(true,true)->has('storage_dir')) {
                if(!file_exists(self::storageDirPath().'/router'))
                    mkdir(self::storageDirPath().'/router',0777,true);

                file_put_contents(self::storageDirPath().'/router/routes-'.Core::getCacheString().'.php',"<?php\n\n\$routes = ".var_export($routes,true).";\n\n?>");
            }
        }
        else require self::storageDirPath().'/router/routes-'.Core::getCacheString().'.php';
        $this->routes = $routes;

    }

    /**
     * @param LocaleContainerInterface|null $localeContainer
     * @return void
     */
    public function setLocaleContainer(LocaleContainerInterface $localeContainer): void
    {
        $this->localeContainer = $localeContainer;
    }

    /**
     * @return LocaleContainerInterface|null
     */
    public function getLocaleContainer()
    {
        return $this->localeContainer;
    }

    /**
     * @return bool
     */
    public function hasLocaleContainer(): bool
    {
        return  (null !== $this->localeContainer);
    }

    /**
     * @param DatabaseContainerInterface|null $databaseContainer
     * @return void
     */
    public function setDatabaseContainer(DatabaseContainerInterface $databaseContainer): void
    {
        $this->databaseContainer = $databaseContainer;
    }

    /**
     * @return null|DatabaseContainerInterface
     */
    public function getDatabaseContainer()
    {
        return $this->databaseContainer;
    }

    /**
     * @return bool
     */
    public function hasDatabaseContainer():bool
    {
        return !is_null($this->databaseContainer);
    }

    /**
     * @param InstancesContainerInterface|null $instancesContainer
     * @return void
     */
    public function setInstancesContainer(InstancesContainerInterface $instancesContainer): void
    {
        $this->instancesContainer = $instancesContainer;
    }

    /**
     * @return InstancesContainerInterface|null
     */
    public function getInstancesContainer()
    {
        return $this->instancesContainer;
    }

    /**
     * @return bool
     */
    public function hasInstancesContainer(): bool
    {
        return !is_null($this->instancesContainer);
    }

    /**
     * @param RequestContainerInterface $requestContainer
     */
    public function setRequestContainer(RequestContainerInterface $requestContainer): void
    {
        $this->requestContainer = $requestContainer;
    }

    /**
     * @return RequestContainerInterface|null
     */
    public function getRequestContainer()
    {
        return $this->requestContainer;
    }

    /**
     * @return bool
     */
    public function hasRequestContainer(): bool
    {
        return (null !== $this->requestContainer);
    }

    /**
     * @param SiteContainerInterface $siteContainer
     */
    public function setSiteContainer(SiteContainerInterface $siteContainer): void
    {
        $this->siteContainer = $siteContainer;
    }

    /**
     * @return SiteContainerInterface|null
     */
    public function getSiteContainer()
    {
        return $this->siteContainer;
    }

    /**
     * @return bool
     */
    public function hasSiteContainer(): bool
    {
        return (null !== $this->siteContainer);
    }

    /**
     * @param ViewsContainerInterface $viewsContainer
     * @return mixed|void
     */
    public function setViewsContainer(ViewsContainerInterface $viewsContainer): void
    {
        $this->viewsContainer = $viewsContainer;
    }

    /**
     * @return ViewsContainerInterface|null
     */
    public function getViewsContainer()
    {
        return $this->viewsContainer;
    }

    /**
     * @return bool
     */
    public function hasViewsContainer(): bool
    {
        return (null !== $this->viewsContainer);
    }

    /**
     * @param string $className
     * @param string $methodName
     * @param array $chosenHandler
     * @param LanguageInterface $language
     * @param bool $runValidation
     * @return mixed|JsonResponse
     * @throws ReflectionException
     */
    protected function invokeAction(string $className, string $methodName, array $chosenHandler, LanguageInterface $language, bool $runValidation = false)
    {
        $method = new ReflectionMethod($className,$methodName);
        $argVals = [];
        $params = $method->getParameters();
        $validatorErrors = null;

        foreach ($params as $par) {
            $parName = $par->getName();
            if(count($chosenHandler['var'])  > 0 && array_key_exists($parName,$chosenHandler['var'])) {
                if($chosenHandler['var'][$parName]['type'] === 'int')
                    $argVals[] = (int)$chosenHandler['varval'][$chosenHandler['var'][$parName]['position']];
                elseif ($chosenHandler['var'][$parName]['type'] === 'float')
                    $argVals[] = (float)$chosenHandler['varval'][$chosenHandler['var'][$parName]['position']];
                elseif (array_key_exists('value', $chosenHandler['var'][$parName])) {
                    $argVals[] = $chosenHandler['var'][$parName]['value'];
                }
                else
                    $argVals[] = $chosenHandler['varval'][$chosenHandler['var'][$parName]['position']];
            }
            else {
                $parType = $par->getType();
                if(!is_null($parType)) {
                    $parType = $parType->getName();
                    $parTypeParents = Core::getClassParents($parType);
                    if($parType === \Symfony\Component\HttpFoundation\Request::class || $parType === \Maksatech\Http\Requests\CustomValidation\Request::class || in_array(\Symfony\Component\HttpFoundation\Request::class,$parTypeParents) || in_array(\Maksatech\Http\Requests\CustomValidation\Request::class,$parTypeParents)) {
                        if($parType === \Maksatech\Http\Requests\CustomValidation\Request::class || in_array(\Maksatech\Http\Requests\CustomValidation\Request::class,$parTypeParents)) {
                            if(!$this->getInstancesContainer()->hasInstanceOf($parType)) {
                                $app = new Application();
                                $argVal = $parType::createFromGlobals();

                                $argVal->setContainer($app);
                                $locale = $language ? $language->getLocale() : 'en';

                                $validator = new Validator($this->getLocaleContainer()->getTranslatorByLocale($locale, 'validation'), $argVal->all(), $argVal->rules());
                                $databaseContainer = $this->getDatabaseContainer();

                                if ($databaseContainer && $databaseContainer->hasDatabaseCapsule()) {
                                    $databaseCapsule = $databaseContainer->getDatabaseCapsule();
                                    $verifier = new DatabasePresenceVerifier($databaseCapsule->getDatabaseManager());
                                    $validator->setPresenceVerifier($verifier);
                                }

                                $argVal->setValidator($validator);
                                $this->getInstancesContainer()->setInstanceOf($parType, $argVal);

                                if(method_exists($argVal,'setRequestPath')) {

                                    if($this->requestPathDecoded === '') {
                                        $this->requestPathDecoded = '/';
                                        $this->requestPath = '/';
                                    }

                                    $argVal->setRequestPath($this->requestPath);
                                }

                                if(method_exists($argVal,'setSite') && $this->hasSiteContainer() && $this->getSiteContainer()->hasSite()) {
                                    $argVal->setSite($this->getSiteContainer()->getSite());
                                }

                                if(!is_null($language) && method_exists($argVal,'setLanguage')) {
                                    $argVal->setLanguage($language);
                                }

                            }
                            else {
                                $argVal = $this->getInstancesContainer()->getInstanceOf($parType);
                            }

                            if ($runValidation) {
                                try {
                                    $argVal->validateResolved();
                                } catch (CustomValidationException $e) {
                                    $validatorErrors = $e->errors();
                                    break;
                                }
                            }

                        }
                        else {

                            if(!$this->getInstancesContainer()->hasInstanceOf($parType)) {
                                $argVal = $parType::createFromGlobals();
                                $this->getInstancesContainer()->setInstanceOf($parType, $argVal);

                                if(method_exists($argVal,'setRequestPath')) {

                                    if($this->requestPathDecoded === '') {
                                        $this->requestPathDecoded = '/';
                                        $this->requestPath = '/';
                                    }

                                    $argVal->setRequestPath($this->requestPath);
                                }

                                if(method_exists($argVal,'setSite') && $this->hasSiteContainer() && $this->getSiteContainer()->hasSite()) {
                                    $argVal->setSite($this->getSiteContainer()->getSite());
                                }

                                if(!is_null($language) && method_exists($argVal,'setLanguage')) {
                                    $argVal->setLanguage($language);
                                }

                            }
                            else {
                                $argVal = $this->getInstancesContainer()->getInstanceOf($parType);
                            }

                        }

                        $argVals[] = $argVal;
                    }
                    elseif($par->isDefaultValueAvailable()) {
                        $argVals[] = $par->getDefaultValue();
                    }
                    elseif ($parType === Theme::class) {
                        /**
                         * @var Theme $theme
                         */
                        $theme = null;

                        if($this->hasInstancesContainer()) {
                            if(!$this->getInstancesContainer()->hasInstanceOf(Theme::class)) {
                                if($this->hasSiteContainer() && $this->getSiteContainer()->hasSite()) {
                                    $this->getInstancesContainer()->setInstanceOf(Theme::class, new Theme($this->getSiteContainer()->getSite(), $className . '::' . $methodName, $this->getViewsContainer()));
                                    $theme = $this->getInstancesContainer()->getInstanceOf(Theme::class);
                                }
                            }
                            else {
                                $theme = $this->getInstancesContainer()->getInstanceOf(Theme::class);
                                $theme->setAction($className . '::' . $methodName);
                            }
                        }

                        $argVals[] = $theme;
                    }
                    else {

                        $argVal = $this->getInstancesContainer()->getInstanceOf($parType);

                        $argVals[] = $argVal;

                    }
                }
                elseif($par->isDefaultValueAvailable()) {
                    $argVals[] = $par->getDefaultValue();
                }
            }
        }

        if(!is_null($validatorErrors)) {
            return new JsonResponse([
                'status' => AbstractController::STATUS_FAIL,
                'messages' => $validatorErrors
            ], 422);
        }
        else {
            return $method->invokeArgs($this->getInstancesContainer()->getInstanceOf($className),$argVals);
        }
    }

    /**
     * @param string $className
     * @param string $methodName
     * @param $response
     * @throws Exception
     */
    protected function responseSend(string $className, string $methodName, &$response)
    {
        if(null !== $response) {
            if(!is_object($response)) {
                if(is_array($response)) {

                }
                else echo $response;
            }
            else {
                $respClass = get_class($response);
                $respParents = self::getClassParents($respClass);
                if(in_array($respClass,[Response::class,\Symfony\Component\HttpFoundation\Response::class])
                    || in_array(Response::class,$respParents)
                    || in_array(\Symfony\Component\HttpFoundation\Response::class,$respParents)) {
                    $response->send();
                }
                else throw new Exception("Error: the '".$methodName."' handler in '".$className."' action return not valid response");
            }
        }
        else throw new Exception("Error: the '".$methodName."' handler in '".$className."' action must return response");
    }

    /**
     * @param string $uri
     * @throws ReflectionException
     * @throws Exception
     */
    public function request(string $uri)
    {
        $uriExpl = explode('?',$uri,2);
        $this->requestPath = $uriExpl[0];

        if($this->requestPath[mb_strlen($this->requestPath)-1] == '/') {
            $this->requestPath = mb_substr($this->requestPath,0,mb_strlen($this->requestPath)-1);
        }
        $this->requestPath = str_replace('//','/',$this->requestPath);

        $this->requestPathDecoded = urldecode($this->requestPath);

        if($this->hasInstancesContainer() && $this->hasRequestContainer()) {
            if(!$this->getInstancesContainer()->hasInstanceOf(IlluminateHttpRequest::class))
                $this->getInstancesContainer()->setInstanceOf(IlluminateHttpRequest::class, IlluminateHttpRequest::createFromGlobals());

            /**
             * @var IlluminateHttpRequest $illuminateRequest
             */
            $illuminateRequest = $this->getInstancesContainer()->getInstanceOf(IlluminateHttpRequest::class);
            $this->getRequestContainer()->setRequest($illuminateRequest);
        }

        if($this->hasRequestContainer() && $this->getRequestContainer()->hasRequest() && $this->hasInstancesContainer() && $this->hasSiteContainer()) {

            $site = null;

            if(self::getConfig(true,true)->has('site') && is_array($siteConfig = self::getConfig(true,true)->get('site'))) {
                $siteRouter = new SiteRouter($siteConfig, $this->getRequestContainer()->getRequest()->getHost());
                $site = $siteRouter->load();
            }

            if($site)
                $this->getSiteContainer()->setSite($site);
        }

        $lang = null;
        if($this->hasRequestContainer() && $this->getRequestContainer()->hasRequest() && self::getConfig(true,true)->has('language') && is_array($languageConfig = self::getConfig(true,true)->get('language'))) {
            $languageRouter = new LanguageRouter($languageConfig, $this->getRequestContainer()->getRequest()->getHost(), $this->requestPathDecoded);
            $lang = $languageRouter->load();

            if(!is_null($lang) && !$lang->isDefault() && $languageRouter->withPathPattern()) {
                $this->requestPathDecoded = mb_substr($this->requestPathDecoded, mb_strlen($lang->getUri()) + 1);
                $this->requestPath = urlencode($this->requestPathDecoded);
            }

        }

        if($this->hasLocaleContainer()) {
            $this->getLocaleContainer()->setLanguage($lang);
        }

        $find = false;
        $chosenHandler = null;

        $requestMethod = mb_strtolower($_SERVER['REQUEST_METHOD']);

        foreach ($this->routes['usual'] as $route => $handler) {
            if($handler['path'] == '*' && (null === $handler['method'] || in_array($requestMethod, $handler['method']))) {
                $chosenHandler = $handler;
                $chosenHandler['name'] = $route;
                $chosenHandler['regular'] = false;
                $chosenHandler['varval'] = [];
            }
            if($handler['path'] === $this->requestPathDecoded && (null === $handler['method'] || in_array($requestMethod, $handler['method']))) {
                $find = true;
                $chosenHandler = $handler;
                $chosenHandler['name'] = $route;
                $chosenHandler['regular'] = false;
                $chosenHandler['varval'] = [];
            }
            if($find)
                break;
        }

        if(!$find) {
            $preg_val = [];
            foreach ($this->routes['regular'] as $route => $handler) {
                if((null === $handler['method'] || in_array($requestMethod, $handler['method'])) && preg_match($handler['path'],$this->requestPathDecoded,$preg_val)) {
                    $chosenHandler = $handler;
                    $chosenHandler['varval'] = [];
                    $chosenHandler['name'] = $route;
                    $chosenHandler['regular'] = true;
                    $find = true;
                    for($i = 1; $i<count($preg_val); $i++)
                        $chosenHandler['varval'][] = $preg_val[$i];
                }
                if($find)
                    break;
            }
        }

        if(!is_null($chosenHandler)) {

            $response = null;
            if(null !== $chosenHandler['middleware']) {
                if(!is_array($chosenHandler['middleware']))
                    $chosenHandler['middleware'] = [$chosenHandler['middleware']];

                foreach ($chosenHandler['middleware'] as $middleware) {
                    if(!is_array($middleware)) {
                        $middleware = [
                            'middleware' => $middleware,
                            'var' => []
                        ];
                    }

                    $middlewareExplode = explode('::', $middleware['middleware'], 2);
                    $className = str_replace('/','\\',trim($middlewareExplode[0]));

                    if(!class_exists($className)) {
                        throw new MiddlewareClassNotFoundException($className);
                    }

                    $methodName = (count($middlewareExplode) > 1) ? trim($middlewareExplode[1]): 'handle';

                    if(!method_exists($className, $methodName)) {
                        throw new MiddlewareMethodNotFoundException($className, $methodName);
                    }

                    if(count($middleware['var']) > 0) {
                        $chosenHandlerCopy = $chosenHandler;
                        foreach ($middleware['var'] as $varName => $varValue) {
                            if(array_key_exists($varName, $chosenHandlerCopy['var'])) {
                                $chosenHandlerCopy['varval'][$chosenHandlerCopy['var'][$varName]['position']] = $varValue;
                            }
                            else {
                                $chosenHandlerCopy['varval'][] = $varValue;
                                $chosenHandlerCopy['var'][$varName] = [
                                    'type' => null,
                                    'position' => count($chosenHandlerCopy['varval'])-1
                                ];
                            }
                        }

                        if($response = $this->invokeAction($className, $methodName, $chosenHandlerCopy, $lang)) {
                            $this->responseSend($className, $methodName, $response);
                            break;
                        }
                    }
                    else {
                        if($response = $this->invokeAction($className, $methodName, $chosenHandler, $lang)) {
                            $this->responseSend($className, $methodName, $response);
                            break;
                        }
                    }
                }
            }

            if(null === $response) {
                $chosenHandler['action'] = explode('::',$chosenHandler['action']);

                if(count($chosenHandler['action']) !== 2) {
                    throw new Exception("Error: the route '".$chosenHandler['name']."' is not valid");
                }

                $className = str_replace('/','\\',trim($chosenHandler['action'][0]));
                if(class_exists($className))
                {
                    $methodName = trim($chosenHandler['action'][1]);
                    if(method_exists($className,$methodName)) {
                        $response = $this->invokeAction($className, $methodName, $chosenHandler, $lang, true);
                        $this->responseSend($className, $methodName, $response);
                    }
                    else throw new Exception("Error: the '".$methodName."' handler not exists in the '".$className."' action");
                }
                else throw new Exception("Error: the '".$className."' action not exists in '".$chosenHandler['name']."' route");
            }
        }
        else throw new Exception("Error: default routing not defined");

    }

}