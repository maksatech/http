<?php

namespace Maksatech\Http\Requests;

use Maksatech\Containers\LanguageInterface;
use Maksatech\Http\Requests\Exceptions\InvalidLanguageConfigException;
use Maksatech\Http\Requests\Exceptions\InvalidDependencyConfigException;
use ReflectionException;
use ReflectionClass;

class LanguageRouter extends DependencyRouter
{
    public const ROUTING_PATTERN_PATH = 'path';
    public const ROUTING_PATTERN_HOST = 'host';

    public const ROUTING_PATTERNS = [
      self::ROUTING_PATTERN_PATH,
      self::ROUTING_PATTERN_HOST,
    ];

    protected ?string $host;
    protected ?string $requestPath;

    /**
     * @param array $config
     * @param string|null $host
     * @param string|null $requestPath
     * @throws InvalidDependencyConfigException
     * @throws InvalidLanguageConfigException
     * @throws ReflectionException
     */
    public function __construct(array $config, ?string $host = null, ?string $requestPath = null)
    {
        parent::__construct($config);

        if(array_key_exists('pattern', $config) && !is_null($config['pattern']) && !in_array($config['pattern'], self::ROUTING_PATTERNS)) {
            throw new InvalidLanguageConfigException('`pattern` setting contains a non-existent routing pattern');
        }

        if(array_key_exists('class', $config) && !is_null($config['class'])) {
            $reflection = new ReflectionClass($config['class']);

            if(!$reflection->implementsInterface(LanguageInterface::class)) {
                throw new  InvalidLanguageConfigException($reflection->getName()." class not implements " . LanguageInterface::class);
            }
        }

        $this->config['pattern'] = $config['pattern'] ?? self::ROUTING_PATTERN_HOST;

        if(is_null($this->config['pattern'])) {
            $this->config['pattern'] = self::ROUTING_PATTERN_HOST;
        }

        if($this->config['pattern'] === self::ROUTING_PATTERN_HOST && is_null($host)) {
            throw new InvalidLanguageConfigException('`pattern` setting contains `host` value but `$host` parameter is null');
        }

        if($this->config['pattern'] === self::ROUTING_PATTERN_PATH && is_null($requestPath)) {
            throw new InvalidLanguageConfigException('`pattern` setting contains `path` value but `$requestPath` parameter is null');
        }

        $this->host = $host;
        $this->requestPath = $requestPath;
    }

    /**
     * @return string
     */
    public function getRoutingPattern(): string
    {
        return $this->config['pattern'];
    }

    /**
     * @return bool
     */
    public function withHostPattern(): bool
    {
        return $this->getRoutingPattern() === self::ROUTING_PATTERN_HOST;
    }

    /**
     * @return bool
     */
    public function withPathPattern(): bool
    {
        return $this->getRoutingPattern() === self::ROUTING_PATTERN_PATH;
    }

    /**
     * @param string $languageClass
     * @param string $requestPath
     * @return LanguageInterface|null
     */
    protected static function loadByRequestPath(string $languageClass, string $requestPath): ?LanguageInterface
    {
        if($requestPath !== '') {
            $pathExplode = explode('/',$requestPath,3);
            if(mb_strlen($pathExplode[1]) > 0) {
                $lang = $languageClass::loadByUri($pathExplode[1],true);

                if(!is_null($lang)) {
                    return $lang;
                }
            }
        }

        return $languageClass::loadDefault();
    }

    /**
     * @param string $languageClass
     * @param string $host
     * @return LanguageInterface|null
     */
    protected static function loadByHost(string $languageClass, string $host): ?LanguageInterface
    {

        $hostExplode = explode('.',$host,2);

        if(count($hostExplode) > 1 && $hostExplode[0] !== 'www') {
            $lang = $languageClass::loadByUri($hostExplode[0],true);

            if(!is_null($lang)) {
                return $lang;
            }
        }

        return $languageClass::loadDefault();
    }

    /**
     * @return LanguageInterface|null
     */
    public function load(): ?LanguageInterface
    {
        if(is_null($this->getClass())) {
            return null;
        }

        if($this->getRoutingPattern() === self::ROUTING_PATTERN_PATH) {
            return self::loadByRequestPath($this->getClass(), $this->requestPath);
        }

        return self::loadByHost($this->getClass(), $this->host);
    }
}