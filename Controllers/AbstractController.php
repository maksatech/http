<?php
namespace Maksatech\Http\Controllers;

use Maksatech\Core\Core;

/**
 * Class AbstractController
 * @package Maksatech\Http\Controllers
 */
abstract class AbstractController extends Core
{

    public const STATUS_OK = 'ok';
    public const STATUS_FAIL = 'fail';

    /**
     * AbstractController constructor.
     */
    function __construct()
    {
        parent::__construct();
    }

    function __destruct()
    {
        parent::__destruct();
    }
}