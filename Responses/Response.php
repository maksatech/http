<?php

namespace Maksatech\Http\Responses;


use Maksatech\Core\Core;

/**
 * Class Response
 * @package Maksatech\Http\Responses
 */
class Response extends Core
{
    const DEFAULT_RESPONSE_CODE = 200;
    const DEFAULT_RESPONSE_TYPE = 'text/plain';

    /**
     * @var string
     */
    protected $contentType;

    /**
     * @var string
     */
    protected $charset;

    /**
     * @var string
     */
    protected $body;

    /**
     * @var string
     */
    protected $statusString;

    /**
     * @var int
     */
    protected $responseCode;

    /**
     * Response constructor.
     * @param string $body
     * @param string $contentType
     * @param string $charset
     * @param int $responseCode
     */
    function __construct(string &$body, string $contentType = Response::DEFAULT_RESPONSE_TYPE, string $charset = 'utf-8', int $responseCode = Response::DEFAULT_RESPONSE_CODE)
    {
        parent::__construct();

        $this->body = $body;

        if(!is_null($contentType))
            $this->contentType = $contentType;
        else
            $this->contentType = static::DEFAULT_RESPONSE_TYPE;

        $this->charset = strtolower($charset);
        $this->setResponseCode($responseCode);
    }

    function __destruct()
    {
        parent::__destruct();
    }

    /**
     * @return void
     */
    public function send()
    {
        header($this->statusString);
        header('Content-Type: '.$this->contentType.'; charset='.$this->charset);
        echo $this->body;
    }

    /**
     * @param string $value
     */
    public function setContentType(string $value)
    {
        $this->contentType = $value;
    }

    /**
     * @param int $value
     * @return void
     */
    public function setResponseCode(int $value)
    {
        switch ($value) {
            case 200:
                $this->statusString = "HTTP/1.1 200 OK";
                $this->responseCode = 200;
                break;
            case 401:
                $this->statusString = "HTTP/1.1 401 Unauthorized";
                $this->responseCode = 401;
                break;
            case 403:
                $this->statusString = "HTTP/1.1 403 Forbidden";
                $this->responseCode = 403;
                break;
            case 404:
                $this->statusString = "HTTP/1.1 404 Not Found";
                $this->responseCode = 404;
                break;
            case 500:
                $this->statusString = "HTTP/1.1 500 Internal Server Error";
                $this->responseCode = 500;
                break;
            case 501:
                $this->statusString = "HTTP/1.1 501 Not Implemented";
                $this->responseCode = 501;
                break;
            case 502:
                $this->statusString = "HTTP/1.1 502 Bad Gateway";
                $this->responseCode = 502;
                break;
            case 503:
                $this->statusString = "HTTP/1.1 503 Service Unavailable";
                $this->responseCode = 503;
                break;
            default:
                $this->statusString = "HTTP/1.1 200 OK";
                $this->responseCode = 200;
        }
    }
}