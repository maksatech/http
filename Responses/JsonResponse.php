<?php

namespace Maksatech\Http\Responses;

/**
 * Class JsonResponse
 * @package Maksatech\Http\Responses
 */
class JsonResponse extends Response
{
    /**
     * JsonResponse constructor.
     * @param $data
     * @param int $options
     * @param int $depth
     * @param int $responseCode
     * @param string $charset
     */
    function __construct($data, int $options = 0, int $depth = 512, int $responseCode = Response::DEFAULT_RESPONSE_CODE, string $charset = 'utf-8')
    {
        $jsonString = json_encode($data,$options,$depth);
        parent::__construct($jsonString,'application/x-javascript',$charset,$responseCode);
    }

    function __destruct()
    {
        parent::__destruct();
    }
}