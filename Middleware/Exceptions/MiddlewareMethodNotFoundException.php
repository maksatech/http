<?php
namespace Maksatech\Http\Middleware\Exceptions;

use Maksatech\Core\Exceptions\MethodNotFoundException;
use Throwable;

/**
 * Class MiddlewareMethodNotFoundException
 * @package Maksatech\Http\Middleware\Exceptions
 */
class MiddlewareMethodNotFoundException extends MethodNotFoundException
{
    /**
     * MiddlewareMethodNotFoundException constructor.
     * @param string $middlewareClassName
     * @param string $middlewareMethodName
     * @param Throwable|null $previous
     */
    public function __construct(string $middlewareClassName, string $middlewareMethodName, Throwable $previous = null)
    {
        parent::__construct('middleware '.$middlewareClassName, $middlewareMethodName, $previous);
    }

    public function __destruct()
    {
        parent::__destruct();
    }
}