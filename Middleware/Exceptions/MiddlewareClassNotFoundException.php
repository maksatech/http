<?php
namespace Maksatech\Http\Middleware\Exceptions;

use Maksatech\Core\Exceptions\ClassNotFoundException;
use Throwable;

/**
 * Class MiddlewareClassNotFoundException
 * @package Maksatech\Http\Middleware\Exceptions
 */
class MiddlewareClassNotFoundException extends ClassNotFoundException
{
    /**
     * MiddlewareClassNotFoundException constructor.
     * @param string $middlewareClassName
     * @param Throwable|null $previous
     */
    public function __construct(string $middlewareClassName, Throwable $previous = null)
    {
        parent::__construct($middlewareClassName, $previous);
    }

    public function __destruct()
    {
        parent::__destruct();
    }
}